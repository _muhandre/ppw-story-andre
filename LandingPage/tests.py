from django.test import TestCase, Client
from django.urls import resolve
from .views import index

# Create your tests here.
class LandingPageUnitTest(TestCase):
    
    def test_landingpage_index_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_landingpage_index_url_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_landingpage_index_url_using_landingpage_index_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'LandingPage/index.html')