from django.contrib import admin
from django.urls import path
from .views import index

app_name = 'LandingPage'

urlpatterns = [
    path('', index, name='LandingPage-index'),
]
