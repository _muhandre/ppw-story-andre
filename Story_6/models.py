from django.db import models

# Create your models here.
class Kegiatan(models.Model):
    nama_kegiatan = models.CharField(max_length = 120)
    tanggal_kegiatan = models.DateTimeField(['%m/%d/%Y %H:%M'])
    detail_kegiatan = models.TextField(max_length = 1000)

class Peserta(models.Model):
    nama_peserta = models.CharField(max_length = 120)
    id_line = models.CharField(max_length = 20, unique = True)
    kegiatan = models.ForeignKey(Kegiatan, on_delete = models.CASCADE)