from django.contrib import admin
from django.urls import path
from .views import index, tambah_kegiatan, form_tambah_peserta, tambah_peserta

app_name = 'Story_6'

urlpatterns = [
    path('', index, name='Story_6-index'),
    path('tambah-kegiatan', tambah_kegiatan, name='Story_6-tambah_kegiatan'),
    path('form-tambah-peserta/<int:id>', form_tambah_peserta, name='Story_6-form_tambah_peserta'),
    path('form-tambah-peserta/tambah-peserta/<int:id>', tambah_peserta, name='Story_6-tambah_kegiatan')
]
