# Create your tests here.
from django.test import TestCase, Client
from django.urls import resolve
from .views import index, tambah_kegiatan, form_tambah_peserta, tambah_peserta
from .models import Kegiatan, Peserta
from .forms import Input_Kegiatan, Input_Peserta

class Story5UnitTest(TestCase):
    
    def test_story6_index_url_is_exist(self):
        response = Client().get('/Story_6/')
        self.assertEqual(response.status_code, 200)

    def test_story6_index_url_using_index_func(self):
        found = resolve('/Story_6/')
        self.assertEqual(found.func, index)

    def test_story6_index_url_using_story6_index_template(self):
        response = Client().get('/Story_6/')
        self.assertTemplateUsed(response, 'Story_6/index.html')
    
    def test_Kegiatan_can_create_new_kegiatan(self):
        kegiatan = Kegiatan.objects.create(
            nama_kegiatan = "nama kegiatan",
            tanggal_kegiatan = "2020-12-20 08:00",
            detail_kegiatan = "detail kegiatan"
        )
        jumlah_semua_kegiatan = Kegiatan.objects.all().count()
        self.assertEqual(jumlah_semua_kegiatan, 1)

    def test_Peserta_can_create_new_peserta(self):
        kegiatan = Kegiatan.objects.create(
            nama_kegiatan = "nama kegiatan",
            tanggal_kegiatan = "2020-12-20 08:00",
            detail_kegiatan = "detail kegiatan"
        )
        peserta = Peserta.objects.create(
            nama_peserta = "nama peserta",
            id_line = "id line",
            kegiatan = kegiatan
        )
        jumlah_semua_peserta = Peserta.objects.all().count()
        self.assertEqual(jumlah_semua_peserta, 1)

    def test_story5_form_tambah_peserta_is_right(self):
        kegiatan = Kegiatan.objects.create(
            id = 1,
            nama_kegiatan = "nama kegiatan",
            tanggal_kegiatan = "2020-12-20 08:00",
            detail_kegiatan = "detail kegiatan"
        )
        response = Client().post('/Story_6/form-tambah-peserta/1')
        found = resolve('/Story_6/form-tambah-peserta/1')
        html_response = response.content.decode('utf8')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(found.func, form_tambah_peserta)
        self.assertTemplateUsed(response, 'Story_6/formTambahPeserta.html')
        self.assertIn("nama kegiatan", html_response)
    
    def test_story6_tambah_kegiatan_url_can_save_POST_request(self):
        data = {
            'nama_kegiatan' : "nama kegiatan",
            'tanggal_kegiatan' : "2020-12-20 08:00",
            'detail_kegiatan' : "detail kegiatan"
        }
        response = Client().post(
            '/Story_6/tambah-kegiatan',
            data
        )
        
        jumlah_semua_kegiatan = Kegiatan.objects.all().count()
        self.assertEqual(jumlah_semua_kegiatan, 1)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/Story_6/')

        response_baru = Client().get('/Story_6/')
        html_response = response_baru.content.decode('utf8')
        self.assertIn('nama kegiatan', html_response)
    
    def test_story6_tambah_peserta_url_can_save_GET_request(self):
        kegiatan = Kegiatan.objects.create(
            id = 1, 
            nama_kegiatan = "nama kegiatan",
            tanggal_kegiatan = "2020-12-20 08:00",
            detail_kegiatan = "detail kegiatan"
        )
        data = {
            'nama_peserta' : "nama peserta",
            'id_line' : "id line"
        }
        response = Client().post(
            '/Story_6/form-tambah-peserta/tambah-peserta/1',
            data
        )
        
        jumlah_semua_peserta = Peserta.objects.all().count()
        self.assertEqual(jumlah_semua_peserta, 1)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/Story_6/')

        response_baru = Client().get('/Story_6/')
        html_response = response_baru.content.decode('utf8')
        self.assertIn('nama peserta', html_response)