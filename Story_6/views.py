from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from .forms import Input_Kegiatan, Input_Peserta
from .models import Kegiatan, Peserta

# Create your views here.
def index(request):
    semua_kegiatan = Kegiatan.objects.all()
    list_kegiatan = []
    for kegiatan in semua_kegiatan:
        list_kegiatan.append({
            'id' : kegiatan.id,
            'nama_kegiatan' : kegiatan.nama_kegiatan,
            'tanggal_kegiatan' : kegiatan.tanggal_kegiatan,
            'detail_kegiatan' : kegiatan.detail_kegiatan,
            'peserta_kegiatan' : kegiatan.peserta_set.all()
        })
    context = {
        'input_kegiatan' : Input_Kegiatan,
        'list_kegiatan' : list_kegiatan 
    }
    return render(request, "Story_6/index.html", context)

def tambah_kegiatan(request):
    kegiatan = Input_Kegiatan(request.POST or None)
    if(kegiatan.is_valid() and request.method == 'POST'):
        kegiatan.save()
    return HttpResponseRedirect('/Story_6/')

def form_tambah_peserta(request, id):
    kegiatan = get_object_or_404(Kegiatan, id = id)
    context = {
        'kegiatan' : kegiatan,
        'input_peserta' : Input_Peserta
    }
    return render(request, "Story_6/formTambahPeserta.html", context)

def tambah_peserta(request, id):
    kegiatan = get_object_or_404(Kegiatan, id = id)
    data_peserta = Input_Peserta(request.POST or None)
    if(data_peserta.is_valid() and request.method == 'POST'):
        peserta = Peserta.objects.create(nama_peserta = request.POST['nama_peserta'], id_line = request.POST['id_line'], kegiatan = kegiatan)
        peserta.save()
    return HttpResponseRedirect('/Story_6/')