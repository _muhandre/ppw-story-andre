from django import forms
from .models import Kegiatan, Peserta

class Input_Kegiatan(forms.ModelForm):
    class Meta:
        model = Kegiatan
        fields = ['nama_kegiatan', 'tanggal_kegiatan', 'detail_kegiatan']

    input_attrs = {
        'type' : 'text',
        'required' : True,
    }
    nama_kegiatan = forms.CharField(max_length = 120, widget = forms.TextInput(attrs = input_attrs))
    tanggal_kegiatan = forms.DateTimeField(input_formats = ['%m/%d/%Y %H:%M'], required = True)
    detail_kegiatan = forms.CharField(max_length = 1000, widget = forms.TextInput(attrs = input_attrs))

class Input_Peserta(forms.ModelForm):
    class Meta:
        model = Peserta
        fields = ['nama_peserta', 'id_line']

    input_attrs = {
        'type' : 'text',
        'required' : True,
    }

    nama_peserta = forms.CharField(max_length = 120, widget = forms.TextInput(attrs = input_attrs))
    id_line = forms.CharField(max_length = 20, widget = forms.TextInput(attrs = input_attrs))