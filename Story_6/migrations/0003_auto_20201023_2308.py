# Generated by Django 3.1.2 on 2020-10-23 16:08

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Story_6', '0002_auto_20201023_2242'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Orang',
            new_name='Peserta',
        ),
        migrations.RenameField(
            model_name='peserta',
            old_name='nama_orang',
            new_name='nama_peserta',
        ),
    ]
