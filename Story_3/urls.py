from django.contrib import admin
from django.urls import path
from .views import index, about, exp, photoPortfolio

app_name = 'Story_3'

urlpatterns = [
    path('', index, name='Story_3-index'),
    path('about/', about, name='Story_3-about'),
    path('experience/', exp, name='Story_3-exp'),
    path('photoPortfolio/', photoPortfolio, name='Story_3-photoPortfolio'),
]
