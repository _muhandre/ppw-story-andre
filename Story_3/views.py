from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request,"Story_3/index.html")

def about(request):
    return render(request,"Story_3/about.html")

def exp(request):
    return render(request,"Story_3/exp.html")

def photoPortfolio(request):
    return render(request,"Story_3/photoPortfolio.html")