# Create your tests here.
from django.test import TestCase, Client
from django.urls import resolve
from .views import index, about, exp, photoPortfolio

class Story3UnitTest(TestCase):
    
    def test_story3_index_url_is_exist(self):
        response = Client().get('/Story_3/')
        self.assertEqual(response.status_code, 200)

    def test_story3_index_url_using_index_func(self):
        found = resolve('/Story_3/')
        self.assertEqual(found.func, index)

    def test_story3_index_url_using_story3_index_template(self):
        response = Client().get('/Story_3/')
        self.assertTemplateUsed(response, 'Story_3/index.html')
    
    def test_story3_about_url_is_exist(self):
        response = Client().get('/Story_3/about/')
        self.assertEqual(response.status_code, 200)

    def test_story3_about_url_using_about_func(self):
        found = resolve('/Story_3/about/')
        self.assertEqual(found.func, about)

    def test_story3_about_url_using_story3_about_template(self):
        response = Client().get('/Story_3/about/')
        self.assertTemplateUsed(response, 'Story_3/about.html')
    
    def test_story3_experience_url_is_exist(self):
        response = Client().get('/Story_3/experience/')
        self.assertEqual(response.status_code, 200)

    def test_story3_experience_url_using_exp_func(self):
        found = resolve('/Story_3/experience/')
        self.assertEqual(found.func, exp)

    def test_story3_experience_url_using_story3_exp_template(self):
        response = Client().get('/Story_3/experience/')
        self.assertTemplateUsed(response, 'Story_3/exp.html')
    
    def test_story3_photoPortfolio_url_is_exist(self):
        response = Client().get('/Story_3/photoPortfolio/')
        self.assertEqual(response.status_code, 200)

    def test_story3_photoPortfolio_url_using_photoPortfolio_func(self):
        found = resolve('/Story_3/photoPortfolio/')
        self.assertEqual(found.func, photoPortfolio)

    def test_story3_photoPortfolio_url_using_story3_photoPortfolio_template(self):
        response = Client().get('/Story_3/photoPortfolio/')
        self.assertTemplateUsed(response, 'Story_3/photoPortfolio.html')