from django import forms
from .models import ListMataKuliah

class Input_Form(forms.ModelForm):
    class Meta:
        model = ListMataKuliah
        fields = ['nama_matkul', 'dosen_pengajar', 'sks', 'deskripsi_matkul', 'semester_tahun', 'kelas']

        input_attrs = {
            'type' : 'text',
            'required' : True,
            'placeholder' : 'Ketikkan disini'
        }

        nama_matkul = forms.CharField(max_length = 35, widget = forms.TextInput(attrs = input_attrs))
        dosen_pengajar = forms.CharField(max_length = 50, widget = forms.TextInput(attrs = input_attrs))
        sks = forms.IntegerField(required = True)
        deskripsi_matkul = forms.CharField(max_length = 1000, widget = forms.TextInput(attrs = input_attrs))
        semester_tahun = forms.CharField(max_length = 9, widget = forms.TextInput(attrs = input_attrs))
        kelas = forms.CharField(max_length = 30, widget = forms.TextInput(attrs = input_attrs))