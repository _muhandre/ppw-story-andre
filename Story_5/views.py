from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from .forms import Input_Form
from .models import ListMataKuliah

# Create your views here.
def index(request):
    list_matkul = ListMataKuliah.objects.all()
    response = {
        'input_form' : Input_Form,
        'list_matkul' : list_matkul
    }
    return render(request,"Story_5/index.html", response)

def tambah(request):
    form = Input_Form(request.GET or None)
    if(form.is_valid() and request.method == 'GET'):
        form.save()
    return HttpResponseRedirect('/Story_5/')

def hapus(request, id):
    matkul = get_object_or_404(ListMataKuliah, id = id)
    matkul.delete()
    return HttpResponseRedirect('/Story_5/')

def detail_matkul(request, id):
    matkul = get_object_or_404(ListMataKuliah, id = id)
    response = {
        "matkul" : matkul
    }
    return render(request,"Story_5/detailMatkul.html", response)