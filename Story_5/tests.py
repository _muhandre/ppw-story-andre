# Create your tests here.
from django.test import TestCase, Client
from django.urls import resolve
from .views import index, tambah, hapus, detail_matkul
from .models import ListMataKuliah
from .forms import Input_Form

class Story5UnitTest(TestCase):
    
    def test_story5_index_url_is_exist(self):
        response = Client().get('/Story_5/')
        self.assertEqual(response.status_code, 200)

    def test_story5_index_url_using_index_func(self):
        found = resolve('/Story_5/')
        self.assertEqual(found.func, index)

    def test_story5_index_url_using_story5_index_template(self):
        response = Client().get('/Story_5/')
        self.assertTemplateUsed(response, 'Story_5/index.html')
    
    def test_ListMataKuliah_can_create_new_matkul(self):
        matkul = ListMataKuliah.objects.create(
            nama_matkul = "nama matkul",
            dosen_pengajar = "nama dosen pengajar",
            sks = 1,
            deskripsi_matkul = "deskripsi matkul",
            semester_tahun = "semester tahun matkul",
            kelas = "kelas matkul",
        )
        jumlah_semua_matkul = ListMataKuliah.objects.all().count()
        self.assertEqual(jumlah_semua_matkul, 1)

    def test_story5_detailmatkul_is_right(self):
        matkul = ListMataKuliah.objects.create(
            id = 1,
            nama_matkul = "nama matkul",
            dosen_pengajar = "nama dosen pengajar",
            sks = 1,
            deskripsi_matkul = "deskripsi matkul",
            semester_tahun = "semester tahun matkul",
            kelas = "kelas matkul",
        )
        response = Client().get('/Story_5/detailmatkul/1')
        found = resolve('/Story_5/detailmatkul/1')
        html_response = response.content.decode('utf8')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(found.func, detail_matkul)
        self.assertTemplateUsed(response, 'Story_5/detailMatkul.html')
        self.assertIn("nama matkul", html_response)

    def test_story5_hapus_url_can_delete_data_from_database(self):
        matkul = ListMataKuliah.objects.create(
            id = 1,
            nama_matkul = "nama matkul",
            dosen_pengajar = "nama dosen pengajar",
            sks = 1,
            deskripsi_matkul = "deskripsi matkul",
            semester_tahun = "semester tahun matkul",
            kelas = "kelas matkul",
        )
        jumlah_semua_matkul = ListMataKuliah.objects.all().count()
        response = Client().get(
            '/Story_5/hapus/1'
        )
        self.assertEqual(jumlah_semua_matkul - 1, 0)
    
    def test_story5_tambah_url_can_save_GET_request(self):
        data = {
            'nama_matkul' : "nama matkul",
            'dosen_pengajar' : "nama dosen pengajar",
            'sks' : "1",
            'deskripsi_matkul' : "deskripsi matkul",
            'semester_tahun' : "2020/2021",
            'kelas' : "kelas matkul"
        }
        response = Client().get(
            '/Story_5/tambah/',
            data
        )
        
        jumlah_semua_matkul = ListMataKuliah.objects.all().count()
        self.assertEqual(jumlah_semua_matkul, 1)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/Story_5/')

        response_baru = Client().get('/Story_5/')
        html_response = response_baru.content.decode('utf8')
        self.assertIn('nama matkul', html_response)