from django.contrib import admin
from django.urls import path
from .views import index, detail_matkul, tambah, hapus

app_name = 'Story_5'

urlpatterns = [
    path('', index, name='Story_5-index'),
    path('detailmatkul/<int:id>', detail_matkul, name='Story_5-detailmatkul'),
    path('tambah/', tambah),
    path('hapus/<int:id>', hapus, name = 'Story_5-hapusmatkul')
]
