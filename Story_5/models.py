from django.db import models

# Create your models here.
class ListMataKuliah(models.Model):
    nama_matkul = models.CharField(max_length = 35)
    dosen_pengajar = models.CharField(max_length = 50)
    sks = models.PositiveSmallIntegerField()
    deskripsi_matkul = models.TextField(max_length = 1000)
    semester_tahun = models.CharField(max_length = 9)
    kelas = models.CharField(max_length = 30)
    
