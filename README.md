# PPW Story Andre

[![Pipeline](https://gitlab.com/_muhandre/ppw-story-andre/badges/master/pipeline.svg)](https://gitlab.com/_muhandre/ppw-story-andre/pipelines)
[![Coverage](https://gitlab.com/_muhandre/ppw-story-andre/badges/master/coverage.svg)](https://gitlab.com/_muhandre/ppw-story-andre/pipelines)

- Nama          : Muhamad Andre Gunawan
- NPM           : 1906400021
- Kelas         : PPW-D
- Kode Asdos    : AF

### Detail Story
- Story_1 : Build website by HTML [Link Repo](https://gitlab.com/_muhandre/ppw-story1-andre)
- Story_2 : Design Story_3 website [Link figma](https://www.figma.com/file/5QIkuBfTYO1WeMqxzMIyza/PPW-Story-2-Muhamad-Andre-Gunawan-1906400021)
- Story_3 : Responsive website build by HTML, CSS [Link Repo](https://gitlab.com/_muhandre/ppw-story3-andre)
- Story_4 : Make Story_1 and Story_3 build with Django
- Story_5 : Using models.py and forms.py
- Story_6 : Do unit testing of all apps and learn relationships in django models
- Story_7 : Make web more interactive with JQuery
- Story_8 : Do som AJAX Call