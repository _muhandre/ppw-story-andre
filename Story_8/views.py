from django.http import JsonResponse
from django.shortcuts import render
import requests

# Create your views here.
def index(request):
    return render(request, "Story_8/index.html", context = {})

def get_data(request):
    api_url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    api_json = requests.get(api_url).json()
    return JsonResponse({'data' : api_json}, safe = False)
