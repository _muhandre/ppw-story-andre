$(document).ready(
    function(){
        var keyword;
        var status;
        var penulis;

        $.ajax({
            url: "getdata?q=pemrograman",
            type: "GET",
            success: responseExist,
        })
        function responseExist(response) {
            $("#data").empty()
            var aResponse;
            for(var aResponse of response.data.items){
                if(aResponse.volumeInfo.imageLinks){
                    if(aResponse.accessInfo.epub.isAvailable){
                        status = "Tersedia"
                    }
                    else{
                        status = "Kosong"
                    }
                    if(aResponse.volumeInfo.authors){
                        penulis = aResponse.volumeInfo.authors
                    }
                    else{
                        penulis = "-"
                    }
                    $("#data").append(
                        "<div class = 'd-flex flex-column justify-content-center margin-2 books-data'>" +
                            "<img src = " + aResponse.volumeInfo.imageLinks.thumbnail + " alt = 'Gambar buku' style = 'width: 100%; margin-bottom: 10%;'>" + 
                            "<p class = 'montserrat-semibold medium-font text-center'>" + aResponse.volumeInfo.title + "</p>" + 
                                "<p class = 'montserrat-medium medium-font primary-color'>Penulis</p>" +
                                "<p class = 'montserrat-regular medium-font'>" + penulis + "</p>" + 
                                "<p class = 'montserrat-medium medium-font primary-color'>Penerbit</p>" +
                                "<p class = 'montserrat-regular medium-font'>" + aResponse.volumeInfo.publisher + "</p>" + 
                                "<p class = 'montserrat-medium medium-font primary-color'>Rating</p>" +
                                "<p class = 'montserrat-regular medium-font'>" + aResponse.volumeInfo.averageRating + "</p>" + 
                                "<p class = 'montserrat-medium medium-font primary-color'>Status</p>" +
                                "<p class = 'montserrat-regular medium-font'>" + status + "</p>" + 
                                "<a href = " + aResponse.accessInfo.webReaderLink + "><p class = 'montserrat-medium medium-font'>Link</p></a>" +
                        "</div>"
                    )
                }
            }
        }
        $('#search-input').keyup(function() {
            if($('#search-input').val() === ""){
                keyword = "pemrograman"
            }
            else{
                keyword = $(this).val()
            }
            $.ajax({
                url: "getdata?q=" + keyword,
                type: "GET",
                success: responseExist,
            })
        })
    }
);

'use strict';

var searchBox = document.querySelectorAll('.search-box input[type="text"] + span');

searchBox.forEach(elm => {
  elm.addEventListener('click', () => {
    elm.previousElementSibling.value = '';
  });
});