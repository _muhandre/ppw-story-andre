from django.urls import path
from .views import index, get_data

app_name = 'Story_8'

urlpatterns = [
    path('', index, name='Story_8-index'),
    path('getdata', get_data, name = "Story_8-getdata")
]