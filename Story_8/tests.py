from django.test import TestCase, Client
from django.urls import resolve
from .views import index, get_data

# Create your tests here.
class Story8UnitTest(TestCase):
    def test_story8_index_url_is_exist(self):
        response = Client().get('/Story_8/')
        self.assertEqual(response.status_code, 200)

    def test_story8_index_url_using_index_func(self):
        found = resolve('/Story_8/')
        self.assertEqual(found.func, index)

    def test_story8_index_url_using_story8_index_template(self):
        response = Client().get('/Story_8/')
        self.assertTemplateUsed(response, 'Story_8/index.html')
    
    def test_story8_getdata_url_is_exist(self):
        response = Client().get('/Story_8/getdata?q=keyword')
        self.assertEqual(response.status_code, 200)

    def test_story8_getdata_url_using_getdata_func(self):
        found = resolve('/Story_8/getdata')
        self.assertEqual(found.func, get_data)

    def test_story8_get_api_json_successfully(self):
        response = Client().get('/Story_8/getdata?q=keyword')
        json_response = response.content.decode('utf8')
        self.assertIn('totalItems', json_response)