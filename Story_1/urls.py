from django.contrib import admin
from django.urls import path
from .views import index

app_name = 'Story_1'

urlpatterns = [
    path('', index, name='Story_1-index'),
]
