from django.urls import path
from .views import index

app_name = 'Story_7'

urlpatterns = [
    path('', index, name='Story_7-index')
]