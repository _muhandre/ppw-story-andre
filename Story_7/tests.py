from django.test import TestCase, Client
from django.urls import resolve
from .views import index

# Create your tests here.
class Story7UnitTest(TestCase):
    def test_story7_index_url_is_exist(self):
        response = Client().get('/Story_7/')
        self.assertEqual(response.status_code, 200)

    def test_story7_index_url_using_index_func(self):
        found = resolve('/Story_7/')
        self.assertEqual(found.func, index)

    def test_story7_index_url_using_story6_index_template(self):
        response = Client().get('/Story_7/')
        self.assertTemplateUsed(response, 'Story_7/index.html')
    