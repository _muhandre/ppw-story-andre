$(document).ready(
    function(){
        $('.accordion-title').each(function() {
            $(this).mouseover(function() {
                if($(this).find('.description-toggle').html() == "+"){
                    $(this).css('background-color', '#ed6663')
                }
            })
            $(this).mouseout(function() {
                if($(this).find('.description-toggle').html() == "+"){
                    $(this).css('background-color', '#FFA372')
                }
            })
        })
        $('.description-toggle').each(function() {
            $(this).click(function() {
                if($(this).html() == '+'){
                    $(this).html('-');
                    $(this).closest('.accordion-title').next().slideDown();
                    $(this).closest('.accordion-title').css('background-color', '#ed6663');
                }
                else{
                    $(this).html('+');
                    $(this).closest('.accordion-title').next().slideUp();
                    $(this).closest('.accordion-title').css('background-color', '#FFA372');
                }
            });
        })
        $('.move-down').each(function() {
            $(this).click(function() {
                $(this).closest('.accordion-section').next().after(($(this).closest('.accordion-section')));
            })
        })

        $('.move-up').each(function() {
            $(this).click(function() {
                $(this).closest('.accordion-section').prev().before(($(this).closest('.accordion-section')));
            })
        })
    }
);