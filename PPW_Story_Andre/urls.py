"""PPW_Story_Andre URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('LandingPage.urls')),
    path('Story_1/', include('Story_1.urls')),
    path('Story_3/', include('Story_3.urls')),
    path('Story_5/', include('Story_5.urls')),
    path('Story_6/', include('Story_6.urls')),
    path('Story_7/', include('Story_7.urls')),
    path('Story_8/', include('Story_8.urls')),
    path('Story_9/', include('Story_9.urls')),
]
