from django.test import TestCase, Client
from django.urls import resolve
from .views import index, signup_page, auth_login, auth_logout, get_data
from django.contrib.auth.models import User

# Create your tests here.
class Story9UnitTest(TestCase):
    def test_story9_index_url_is_exist(self):
        response = Client().get('/Story_9/')
        self.assertEqual(response.status_code, 200)

    def test_story9_index_url_using_index_func(self):
        found = resolve('/Story_9/')
        self.assertEqual(found.func, index)

    def test_story9_index_url_using_story9_index_template(self):
        response = Client().get('/Story_9/')
        self.assertTemplateUsed(response, 'Story_9/index.html')
    
    def test_story9_signup_url_is_exist(self):
        response = Client().get('/Story_9/signup/')
        self.assertEqual(response.status_code, 200)

    def test_story9_signup_valid(self):
        newUser_data = {
            'username' : "Namacalonuser",
            'email' : "emailcalonuser@gmail.com",
            'password1' : "Passwordcalonuser123",
            'password2' : "Passwordcalonuser123"
        }
        response = Client().post(
            '/Story_9/signup/',
            newUser_data
        )

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/Story_9/signup')
    
    def test_story9_signup_invalid(self):
        newUser_data = {
            'username' : "Nama calon user",
            'email' : "emailcalonuser@gmail.com",
            'password1' : "passwordcalonuser123",
            'password2' : "passwordcalonuser"
        }
        response = Client().post(
            '/Story_9/signup/',
            newUser_data
        )

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/Story_9/signup')
    
    def test_story9_login_url_is_exist(self):
        response = Client().get('/Story_9/login/')
        self.assertEqual(response.status_code, 200)

    def test_story9_login_success(self):
        user = User.objects.create_user(
            username = "Nama user",
            email = "emailuser@gmail.com",
            password = "Passworduser123",
        )
        user.save()
        user_data = {
            'username' : "Nama user",
            'password' : "Passworduser123",
        }
        response = Client().post(
            '/Story_9/login/',
            user_data
        )

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/Story_9')

        response_baru = Client().get('/Story_9/')
        html_response = response_baru.content.decode('utf8')
    
    def test_story9_login_unsuccess(self):
        user = User.objects.create_user(
            username = "Nama user",
            email = "emailuser@gmail.com",
            password = "Passworduser123",
        )
        user.save()
        user_data = {
            'username' : "Nama user beda",
            'password' : "Passworduser123beda",
        }
        response = Client().post(
            '/Story_9/login/',
            user_data
        )

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/Story_9/login')
    
    def test_story9_logout(self):
        user = User.objects.create_user(
            username = "Nama user",
            email = "emailuser@gmail.com",
            password = "Passworduser123",
        )
        user.save()
        user_data = {
            'username' : "Nama user",
            'password' : "Passworduser123",
        }
        response = Client().post(
            '/Story_9/login/',
            user_data
        )

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/Story_9')

        response_baru = Client().get('/Story_9/logout/')
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/Story_9')
    
    def test_story9_getdata_url_is_exist(self):
        response = Client().get('/Story_9/getdata?q=keyword')
        self.assertEqual(response.status_code, 200)

    def test_story9_getdata_url_using_getdata_func(self):
        found = resolve('/Story_9/getdata')
        self.assertEqual(found.func, get_data)

    def test_story9_get_api_json_successfully(self):
        response = Client().get('/Story_9/getdata?q=keyword')
        json_response = response.content.decode('utf8')
        self.assertIn('totalItems', json_response)