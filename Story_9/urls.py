from django.urls import path
from .views import index, signup_page, auth_login, auth_logout, get_data

app_name = 'Story_9'

urlpatterns = [
    path('', index, name='Story_9-index'),
    path('signup/', signup_page, name = 'Story_9-signup'),
    path('login/', auth_login, name='Story_9-login'),
    path('logout/', auth_logout, name='Story_9-logout'),
    path('getdata', get_data, name = "Story_9-getdata")
]