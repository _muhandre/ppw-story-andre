from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from .forms import SignupForm
from django.http import JsonResponse
import requests

def index(request):
    return render(request, "Story_9/index.html", context = {})

def signup_page(request):
    if request.method == "POST":
        form = SignupForm(request.POST)
        if form.is_valid():
            form.save()
            user = form.cleaned_data.get("username")
            messages.success(request, 'Akun dengan username ' + user + " telah berhasil dibuat")
        else:
            messages.error(request, "Data input tidak valid")
        return HttpResponseRedirect('/Story_9/signup')
    else:
        context = {'form' : SignupForm()}
        return render(request, "Story_9/signupPage.html", context)

def auth_login(request):
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username = username, password = password)
        if user is not None:
            login(request, user)
            request.session['user_logged_username'] = username
            request.session['user_logged_email'] = user.email
            return HttpResponseRedirect('/Story_9')
        else:
            messages.error(request, "Username atau password salah")
            return HttpResponseRedirect('/Story_9/login')
    else:
        return render(request, "Story_9/loginPage.html", context = {})

def auth_logout(request):
    logout(request)
    return HttpResponseRedirect('/Story_9/')

def get_data(request):
    api_url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    api_json = requests.get(api_url).json()
    return JsonResponse({'data' : api_json}, safe = False)
